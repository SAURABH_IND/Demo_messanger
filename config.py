import os
basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'
    MYSQL_DATABASE_USER = 'root'
    MYSQL_DATABASE_PASSWORD = 'xxxxxx'
    MYSQL_DATABASE_DB = 'project'
    MYSQL_DATABASE_HOST = 'localhost'


class ProductionConfig(Config):
    DEBUG = False
    MYSQL_DATABASE_USER = 'root'
    MYSQL_DATABASE_PASSWORD = 'xxxxxx'
    MYSQL_DATABASE_DB = 'project'
    MYSQL_DATABASE_HOST = '192.111.0.111'


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    MYSQL_DATABASE_USER = 'root'
    MYSQL_DATABASE_PASSWORD = 'xxxxx'
    MYSQL_DATABASE_DB = 'project'
    MYSQL_DATABASE_HOST = 'localhost'

class TestingConfig(Config):
    TESTING = True
