import MySQLdb

db = MySQLdb.connect("localhost","root","xxxxx","project")
con = db.cursor()


#----------USER'S------------#
con.execute("SET sql_notes = 0; ")
con.execute("CREATE table IF NOT EXISTS user(name varchar(30),email varchar(40) primary key,password varchar(20),image blob)")

#----------	FRIEND LIST---------#
con.execute("SET sql_notes = 0; ")
con.execute("CREATE table IF NOT EXISTS friendlist(id int primary key auto_increment,users varchar(30),friend varchar(30))")


#----------MESSANGER-------------#
con.execute("SET sql_notes = 0; ")
con.execute("CREATE table IF NOT EXISTS messanger(id int primary key auto_increment,admin varchar(30),user varchar(30),message varchar(500))")


#----------REQUEST----------------#
con.execute("SET sql_notes = 0; ")
con.execute("CREATE table IF NOT EXISTS request(user varchar(30),friend varchar(30))")



