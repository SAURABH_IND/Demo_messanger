import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

Base = declarative_base()

class User(Base):
	__tablename__ = 'user'

	email = Column(String(40),primary_key=True)
	name = Column(String(30))
	password = Column(String(20))
	image = Column(String(40))

class Request(Base):
	__tablename__ = 'request'

	id = Column(Integer,primary_key=True)
	user = Column(String(30))
	friend = Column(String(30))

class FriendList(Base):
	__tablename__ = 'friendlist'

	id = Column(Integer,primary_key=True)
	users = Column(String(30),ForeignKey('user.email'))
	user = relationship(User)
	friend = Column(String(30))

class Messanger(Base):
	__tablename__ = 'messanger'

	id = Column(Integer,primary_key=True)
	admin = Column(String(30))
	user = Column(String(30))
	message = Column(String(30))

engine = create_engine('mysql://root:mysql@localhost/project')
Base.metadata.create_all(engine)

