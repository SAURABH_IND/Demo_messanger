from flask import Flask, render_template, redirect, request, url_for, session
import os
import random
import json
from MySQLdb import *
import MySQLdb
import re
import hashlib
from flaskext.mysql import MySQL

app = Flask(__name__)
app.secret_key = os.urandom(24)

mysql = MySQL()
app.config.from_object(os.environ['APP_SETTINGS'])
mysql.init_app(app)
db = mysql.connect()

#db = MySQLdb.connect("localhost","root","XXXXXX","project")

@app.route('/login',methods=['POST','GET'])
def Login():
	if request.method == 'POST':
		email = request.form['email']
		password = request.form['password']
		india = []
		cur = db.cursor()
		cur.execute("SELECT * from user where email=%s and password=%s",(email,password))
		temp = cur.fetchall()

		user=[]
		if temp:
			for i in temp:
				user.append(i)
				session['user'] = i[1]
				session['all'] = user
				return redirect(url_for('Profile'))
		else:
			return redirect(url_for('LoginError'))
	return render_template("/user/login.html")

@app.route('/register',methods=['POST','GET'])
def Register():
	if request.method == 'POST':
		name = request.form['name']
		email = request.form['email']
		password = request.form['password']
		cur = db.cursor()
		cur.execute("SELECT * from user where email=%s",[email])
		temp = cur.fetchall()
		if temp:
			return redirect(url_for('RegisterError'))
		else:
			con = db.cursor()
			con.execute("""INSERT INTO user(name,email,password,image) values(%s,%s,%s,%s)""",(name,email,password,'india.jpeg'))
			db.commit()
			return redirect(url_for('Login'))
	return render_template("/user/register.html")


@app.route('/forget',methods=['POST','GET'])
def Forget():
	if request.method == 'POST':
		email = request.form['email']
		mobile = request.form['mobile']
		password = request.form['password']
		cur = db.cursor()
		cur.execute("SELECT * from user where email=%s and mobile=%s",(email,mobile))
		x = cur.fetchall()
		if x:
			con = db.cursor()
			con.execute("UPDATE user set password=%s where email=%s",(password,email))
			db.commit()
			return redirect(url_for('Login'))
		else:
			return redirect(url_for('LoginError'))
	return render_template("/user/forget.html")




@app.route('/profile',methods=['GET'])
def Profile():
	if 'user' in session:
		requestout=[]
		requestin=[]
		con = db.cursor()
		con.execute("SELECT * from request where user=%s",[session['user']])
		x = con.fetchall()
		for i in x:
			requestout.append(i)

		cur = db.cursor()
		cur.execute("SELECT * from request where friend=%s",[session['user']])
		y = cur.fetchall()
		for j in y:
			requestin.append(j)

		return render_template('/user/request.html',user=session['all'],requestout=requestout,requestin=requestin)
	return redirect(url_for('Login'))

@app.route('/request',methods=['POST','GET'])
def User_request():
	if 'user' in session:
		if request.method == 'POST':
			name = request.form['name']
			cur1 = db.cursor()
			cur1.execute("SELECT * from user where email=%s",[name])
			temp3 = cur1.fetchall()

			if temp3:
				con = db.cursor()
				con.execute("SELECT * from friendlist where friend=%s and users=%s",(name,session['user']))
				temp1 = con.fetchall()

				if temp1:
					return redirect(url_for('Profile'))
				else:
					cur = db.cursor()
					cur.execute("SELECT * from request where (user=%s and friend=%s) or (user=%s and friend=%s)",(session['user'],name,name,session['user']))
					temp2 = cur.fetchall()

					if temp2:
						return redirect(url_for('Profile'))
					else:
						cur2=db.cursor()
						cur2.execute("""INSERT INTO request(user,friend) values(%s,%s)""",(session['user'],name))
						db.commit()
			else:
				return redirect(url_for('Profile'))
			
		return redirect(url_for('Profile'))
	return redirect(url_for('Login'))


@app.route('/reject/<string:sender>',methods=['GET','POST'])
def Reject(sender):
	if 'user' in session:
		cur = db.cursor()
		cur.execute("DELETE from request where friend=%s and user=%s",(session['user'],sender))
		db.commit()
		return redirect(url_for('Profile'))
	return redirect(url_for('Login'))

@app.route('/cancel/<string:reciever>',methods=['GET','POST'])
def Cancel(reciever):
	if 'user' in session:
		cur = db.cursor()
		cur.execute("DELETE from request where friend=%s and user%s",(reciever,session['user']))
		db.commit()
		return redirect(url_for('Profile'))
	return redirect(url_for('Login'))

@app.route('/accept/<string:sender>',methods=['GET','POST'])
def Accept(sender):
	if 'user' in session:
		cur = db.cursor()
		cur.execute("DELETE from request where friend=%s and user=%s",(session['user'],sender))
		db.commit()

		cur.execute("""INSERT INTO friendlist(users,friend) values(%s,%s)""",(session['user'],sender))
		db.commit()

		cur.execute("""INSERT INTO friendlist(users,friend) values(%s,%s)""",(sender,session['user']))
		db.commit()
		return redirect(url_for('Profile'))
	return redirect(url_for('Login'))


@app.route('/message/<name>',methods=['GET','POST'])
@app.route('/message')
def Message(name=None):
	if 'user' in session:
		friend=[]
		con = db.cursor()
		con.execute("SELECT * from friendlist where users=%s",[session['user']])
		temp = con.fetchall()
		for i in temp:
			cur = db.cursor()
			cur.execute("SELECT * from user where email=%s",[i[2]])
			temp1 = cur.fetchall()
			for j in temp1:
				friend.append(j)

		chat=[]
		message=[]
		if name:
			cur1 = db.cursor()
			cur1.execute("SELECT * from messanger where (admin=%s and user=%s) or (admin=%s and user =%s)",(session['user'],name,name,session['user']))
			temp2 = cur1.fetchall()
			for i in temp2:
				message.append(i)

			cur2 = db.cursor()
			cur2.execute("SELECT * from user where email=%s",[name])
			temp = cur2.fetchall()
			for i in temp:
				chat.append(i)
		else:
			pass
		
		return render_template("/user/message.html",user=session['all'],friend=friend,message=message,chat=chat)
	return redirect(url_for('Login'))

@app.route('/sentmessage/<sender>/<message>',methods=['GET','POST'])
def SentMessage(sender,message):
	if 'user' in session:
		print (message)
		cur = db.cursor()
		cur.execute("""INSERT INTO messanger(admin,user,message) values(%s,%s,%s)""",(session['user'],sender,message))
		db.commit()
		return redirect(url_for('Message',name=sender))
	return redirect(url_for('Login'))



@app.route('/loginerror')
def LoginError():
	return render_template("/error/loginerror.html")

@app.route('/registererror')
def RegisterError():
	return render_template("/error/registererror.html")

@app.route('/logout')
def Logout():
	session.pop('user',None)
	return redirect(url_for('Login'))



if __name__ == "__main__":
	app.run(debug=True)
