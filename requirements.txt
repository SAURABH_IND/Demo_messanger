click==6.7
Flask==0.12.2
itsdangerous==0.24
Jinja2==2.10
MarkupSafe==1.0
mysql-connector-python==8.0.11
MySQL-python==1.2.5
protobuf==3.5.2.post1
PyMySQL==0.8.0
six==1.11.0
SQLAlchemy==1.2.2
Werkzeug==0.14.1
